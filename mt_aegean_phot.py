import click
import os
from mt_fncts import make_png, select_input_objects, make_cutout
from ibastro import fitslibs
import ibcommon.parse as ibparse
from ibphotometry import AegeanPhot
from pprint import pprint

mwa_bands = {'072-080': 76155000, '080-088': 83835000, '072-103': 87675000, '088-095': 91515000,
             '095-103': 99195000, '103-111': 106875000, '111-118': 114555000, '103-134': 118395000,
             '118-126': 122235000, '126-134': 129915000, '139-147': 142715000, '147-154': 150395000,
             '139-170': 154235000, '154-162': 158075000, '162-170': 165755000, '170-177': 173435000,
             '177-185': 181115000, '185-193': 188795000, '193-200': 196475000, '170-231': 200315000,
             '200-208': 204155000, '208-216': 211835000, '216-223': 219515000, '223-231': 227195000}


@click.command()
@click.argument('input_path', nargs=1)
@click.argument('results_path', nargs=1)
@click.option('--obj_id', '-i', default='all')
@click.option('--img_name_suffix', '-s', default=None)
@click.option('--make_pngs', '-m', is_flag=True, default=False)
def cli(input_path, results_path, obj_id, make_pngs, img_name_suffix):
    # convert types
    results = None
    input_objects = select_input_objects(obj_id)

    for curr_object in input_objects:

        for band,freq in mwa_bands.items():

            # define and create folder for cutouts and pngs
            # in the form result_path/idpnmain/
            in_image = "{}{}_{}.fits".format(ibparse.corrpath(input_path), curr_object.idpnmain, band)
            if not os.path.exists(in_image):
                print("{} dows not exist...".format(in_image))
                continue

            bmaj = fitslibs.getHeaderItems(in_image, 'BMAJ')

            # TODO check if image exists

            aeg = AegeanPhot.AegeanPhot(in_image, curr_object, results_path)

            new_results = aeg.run_until_find(bmaj['BMAJ'] * 3600. / 2., 2.)

            new_results['freq'] = freq
            new_results['band'] = band

            if results is None:
                results = new_results
            elif new_results is not None:
                results = results.append(new_results, ignore_index=True)

            pprint(results)
            # if make_pngs and os.path.exists(out_image):
            #     make_png(out_image, curr_object, cutouts_path, img_name_suffix)

    results.to_csv("{}results.csv".format(ibparse.corrpath(results_path)))
