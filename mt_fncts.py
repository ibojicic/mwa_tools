import ibplotting.plotlibs as ibplot
import ibastro.fitslibs as ibfitslibs
from ibmysql.LoadData import LoadData
import ibcommon.parse as ibparse
import ibastro.montages as ibmontages


def select_input_objects(obj_id='all'):
    mysqlc = LoadData()
    table = mysqlc.table
    input_objects = table.select(table.idpnmain, table.draj2000, table.ddecj2000)
    if obj_id != 'all':
        input_objects = input_objects.where(table.idpnmain == obj_id)

    return input_objects


def delete_extra_header(out_image):
    delitems = ['CTYPE3', 'CRVAL3', 'CRPIX3', 'CDELT3', 'CUNIT3',
                'CTYPE4', 'CRVAL4', 'CRPIX4', 'CDELT4', 'CUNIT4',
                'NAXIS3', 'NAXIS4',
                'PC3_1', 'PC4_1', 'PC3_2', 'PC4_2', 'PC1_3', 'PC2_3',
                'PC3_3', 'PC4_3', 'PC1_4', 'PC2_4', 'PC3_4', 'PC4_4',
                'PC03_01', 'PC04_01', 'PC03_02', 'PC04_02', 'PC01_03', 'PC02_03',
                'PC03_03', 'PC04_03', 'PC01_04', 'PC02_04', 'PC03_04', 'PC04_04']

    ibfitslibs.delete_header_item(out_image, delitems)


def make_png(inimage, curr_object, cutouts_path, img_name_suffix):
    out_image = "{}{}_{}.png".format(ibparse.corrpath(cutouts_path), curr_object.idpnmain, img_name_suffix)
    cross = [[curr_object.draj2000, curr_object.ddecj2000, None, 'red']]

    ibplot.photFigure(inimage, out_image,
                      curr_object.draj2000,
                      curr_object.ddecj2000,
                      crosses=cross,
                      percmin=10,
                      percmax=99,
                      add_text="{}".format(curr_object.idpnmain),
                      textsize=18,
                      addBeam=True
                      )


def make_cutout(input_image, out_image, curr_object, image_size):
    ibmontages.FITScutout(input_image, out_image, curr_object.draj2000, curr_object.ddecj2000, image_size)
    delete_extra_header(out_image)
