from setuptools import setup

setup(
    name='mwa_tools',
    version='0.1',
    py_modules=['mt_cut_mosaic','mt_aegean_phot'],
    install_requires=[
        'Click',
    ],
    entry_points={
        'console_scripts': [
            'mt_cut_mosaic=mt_cut_mosaic:cli',
            'mt_aegean_phot=mt_aegean_phot:cli'
        ]},
)
